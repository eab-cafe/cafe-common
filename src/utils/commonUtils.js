export const getPageList = (form, filterPage = null) => {
  let pageList = form.pages || [];
  if (filterPage) {
    pageList = pageList.filter(page => page.displayId !== filterPage.displayId);
  }
  return pageList;
};

export default {
  getPageList
};
