// Drag and Drop Type
export const FORM = "FORM";
export const PAGE = "PAGE";
export const SECTION_CARD = "SECTION_CARD";
export const QUESTION_CARD = "QUESTION_CARD";
export const CHOICES = "CHOICES";

// ! CAVEAT - DisplayEngine
// ! Any changes made in QuestionTypes (new / del var, var name changes) should modify "utils/questionUtils" accordingly.
// Question Type
export const SECTION = "SECTION";
export const TITLE = "TITLE";
export const TEXT_INPUT = "TEXT_INPUT";
export const SELECTOR = "SELECTOR";
export const CHECK_BOX = "CHECK_BOX";
export const DROP_DOWN = "DROP_DOWN";
export const TOGGLE = "TOGGLE";
export const DATE = "DATE";
export const SCALE = "SCALE";
export const SLIDER = "SLIDER";
export const FILE_UPLOAD = "FILE_UPLOAD";
export const CALC_INPUT = "CALC_INPUT";
export const PLAIN_TEXT = "PLAIN_TEXT";
export const HANDWRITING = "HANDWRITING"; // Handwriting Field
export const CONDITIONAL_CHOICE = "CONDITIONAL_CHOICE"; // Conditional Choices
export const CONDITIONAL_CHOICE_2nd = "CONDITIONAL_CHOICE_2nd";
export const BUTTON = "BUTTON";
export const UI_BORDER = "BORDER";
export const RANDOM_NUMBER = "RANDOM_NUMBER";
// Question Type End

// Text input
export const INPUT_LINE = "INPUT_LINE";
export const INPUT_FIELD = "INPUT_FIELD";

export const ANY = "ANY";
export const NUMERIC = "NUMERIC";
export const CHARACTER = "CHARACTER";
export const EMAIL = "EMAIL";
export const NRIC = "NRIC";

// ! CAVEAT - DisplayEngine
// ! Any changes made in "Selector input" (new / del var, var name changes) should modify "utils/questionUtils" accordingly.
// Selector input
export const SINGLE_ANSWER = "SINGLE_ANSWER";
export const MUITIPLE_ANSWER = "MUITIPLE_ANSWER";
export const OPTIONS = "OPTIONS";

// Drop Down
export const CUSTOM = "CUSTOM";
export const COUNTRY = "COUNTRY";

// Date input
export const INPUT_DATE = "INPUT_DATE";
export const INPUT_TIME = "INPUT_TIME";
export const INPUT_DATE_N_TIME = " INPUT_DATE_N_TIME";

// Toggle
export const INPUT_TWOSTEP = "INPUT_STEP_2";
export const INPUT_THREESTEP = "INPUT_STEP_3";

// Calc Field
export const ADD = "ADD";
export const SUBTRACT = "SUBTRACT";
export const BASIC = "BASIC";
export const ADVANCED = "ADVANCED";

// slider
export const SLIDER_BASIC = "BASIC";
export const SLIDER_BASIC_L = "BASIC_L";
export const SLIDER_BASIC_LV = "BASIC_LV";
export const SLIDER_SCALES = "SCALES";
export const SLIDER_CV = "CUSTOM_VALUES";

// Conditional Choices
export const fistColumn = "Choice Number";
export const prodNameColumn = "Choices Name";
export const prodDescColumn = "Description";

// Button
export const REDIRECT_OUT = "REDIRECT_OUT";
export const REDIRECT_IN = "REDIRECT_IN";
