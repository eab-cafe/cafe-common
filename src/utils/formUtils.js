import _ from "lodash";
import utils from "utils";
import {
  PAGE,
  SECTION_CARD,
  QUESTION_CARD,
  CHOICES,
  FORM,
  TEXT_INPUT,
  NUMERIC,
  NRIC,
  EMAIL,
  CHARACTER
} from "../constants/questionType";
import { ARRAY, OBJECT, ARRAYOBJECT } from "../constants/answerType";
import questionUtils from "./questionUtils";

const state = {
  getForm: () => {},
  updateForm: () => {}
};

function init({ getForm, updateForm }) {
  state.getForm = getForm;
  state.updateForm = updateForm;
}

function getForm() {
  return state.getForm();
}

// ! CAVEAT
// ! DATE: 16/9/2019
// ! TYPE: DUPLICATE
// ! PROJECT: CAFE-API
// ! LOCATION: utils/form.util.js
// Move to co-exist project in future if needed
const getFlattenFormItems = (form = null, maxDepth = null) => {
  const pageList = utils.getPageList(form || state.getForm());
  const allItems = _.flattenDeep(
    _.map(pageList, page => {
      if (maxDepth === PAGE) {
        return page;
      }
      const sections = _.map(page.sections, section => {
        if (maxDepth === SECTION_CARD) {
          return section;
        }
        const questions = _.map(section.questions, question => {
          if (maxDepth === QUESTION_CARD) {
            return question;
          }
          const wrappedQuestion = new questionUtils.QuestionAnswer({
            ...question.answers,
            questionType: question.questionType
          });
          let result = null;
          if (!_.isEmpty(wrappedQuestion.value)) {
            switch (wrappedQuestion.valueType) {
              case OBJECT:
                // console.warn(
                //   `${question.questionType}:: OBJECT:: referenceId:: `,
                //   wrappedQuestion.value
                // );
                result = [question, wrappedQuestion.value];
                break;
              case ARRAYOBJECT: {
                // console.warn(
                //   `${question.questionType}:: ARRAYOBJECT:: referenceId:: `,
                //   _.map(wrappedQuestion.value, referenceId => referenceId)
                // );
                const answers = _.map(wrappedQuestion.value, wrappedAnswer => {
                  const { text, ...otherProps } = wrappedAnswer;
                  return {
                    title: text,
                    ...otherProps
                  };
                });
                result = [question, ...answers];
                break;
              }
              case ARRAY: {
                // console.warn(
                //   `${question.questionType}:: ARRAY:: referenceId:: `,
                //   _.map(wrappedQuestion.value, referenceId => referenceId)
                // );
                // return [question, ...wrappedQuestion.value];
                const answers = _.map(wrappedQuestion.value, wrappedAnswer => {
                  const { text, ...otherProps } = wrappedAnswer;
                  return {
                    title: text,
                    ...otherProps
                  };
                });
                result = [question, ...answers];
                break;
              }
              default:
                // console.error(
                //   `${question.questionType}:: using unhandled valueType ${
                //     wrappedQuestion.valueType
                //   }`
                // );
                result = question;
            }
          } else {
            result = question;
          }
          if (
            wrappedQuestion.optionalProps &&
            wrappedQuestion.optionalProps.hasOptions
          ) {
            result.push(wrappedQuestion.optionalProps);
          }
          return result;
        });
        return [section, ...questions];
      });
      return [page, ...sections];
    })
  );
  return allItems;
};

// ! CAVEAT
// ! DATE: 16/9/2019
// ! TYPE: DUPLICATE
// ! PROJECT: CAFE-API
// ! LOCATION: utils/form.util.js
// Move to co-exist project in future if needed
const getReferenceDataRule = (form = null) => {
  if (form) {
    const rules = form.importDataRules || state.getForm().importDataRules;
    return rules;
  }
  return [];
};

const getQuestionIndex = question => {
  const { getQuestionList } = utils;
  const { displayId } = question;
  const myForm = state.getForm();
  const questionList = myForm ? getQuestionList(myForm) : [];
  return _.findIndex(questionList, { displayId });
};

const getTypeByDisplayId = displayId => {
  if (displayId.indexOf("_C") > -1) {
    return CHOICES;
  }
  if (displayId.indexOf("_Q") > -1) {
    return QUESTION_CARD;
  }
  if (displayId.indexOf("_S") > -1) {
    return SECTION_CARD;
  }
  if (displayId.indexOf("_P") > -1) {
    return PAGE;
  }
  return new Error(
    "formUtils:: getTypeByDisplayId:: Invalid displayId:: ",
    displayId
  );
};

const getParentDisplayIdByType = ({ type, displayId }) => {
  const getException = (params = []) => {
    if (_.some(params, param => _.isEmpty(param))) {
      throw new Error(
        "formUtils:: getDisplayIdByType:: getException:: ",
        params
      );
    }
  };
  const seperator = "_";
  const [formId, pageId, sectionId, questionId] = displayId.split(seperator);
  switch (type) {
    case FORM:
      getException([formId]);
      return formId;
    case PAGE:
      getException([formId, pageId]);
      return [formId, pageId].join(seperator);
    case SECTION_CARD:
      getException([formId, pageId, sectionId]);
      return [formId, pageId, sectionId].join(seperator);
    case QUESTION_CARD:
      getException([formId, pageId, sectionId, questionId]);
      return [formId, pageId, sectionId, questionId].join(seperator);
    default:
      throw new Error(
        "formUtils:: getDisplayIdByType:: Not valid type:: ",
        type
      );
  }
};

const editAndUpdateForm = (displayId, newProps = {}) => {
  const seperator = "_";
  const mapProps = (existProps, newProps) => ({
    ...existProps,
    ...newProps
  });

  const newForm = _.cloneDeep(state.getForm());

  // ! Maybe there are better way

  const [formId, pageId, sectionId, questionId] = displayId.split(seperator);
  const displayIdType = getTypeByDisplayId(displayId);

  const pageIndex = _.findIndex(newForm.pages, {
    displayId: [formId, pageId].join(seperator)
  });
  if (displayIdType === PAGE && pageIndex > -1) {
    newForm.pages[pageIndex] = mapProps(newForm.pages[pageIndex], newProps);
    return newForm;
  }
  const sectionIndex = _.findIndex(newForm.pages[pageIndex].sections, {
    displayId: [formId, pageId, sectionId].join(seperator)
  });
  if (displayIdType === SECTION_CARD && sectionIndex > -1) {
    newForm.pages[pageIndex].sections[sectionIndex] = mapProps(
      newForm.pages[pageIndex].sections[sectionIndex],
      newProps
    );
    return newForm;
  }
  const questionIndex = _.findIndex(
    newForm.pages[pageIndex].sections[sectionIndex].questions,
    {
      displayId: [formId, pageId, sectionId, questionId].join(seperator)
    }
  );

  if (displayIdType === QUESTION_CARD && questionIndex > -1) {
    newForm.pages[pageIndex].sections[sectionIndex].questions[
      questionIndex
    ] = mapProps(
      newForm.pages[pageIndex].sections[sectionIndex].questions[questionIndex],
      newProps
    );
    return newForm;
  }
  if (displayIdType === CHOICES) {
    const targetQuestion =
      newForm.pages[pageIndex].sections[sectionIndex].questions[questionIndex];
    const wrappedQuestion = new questionUtils.QuestionAnswer({
      ...targetQuestion.answers,
      questionType: targetQuestion.questionType,
      question: targetQuestion
    });
    switch (wrappedQuestion.valueType) {
      case OBJECT:
        wrappedQuestion.value = {
          [wrappedQuestion.valueType]: { ...wrappedQuestion.value, ...newProps }
        };
        break;
      case ARRAY:
      case ARRAYOBJECT:
        {
          const answerIndex = _.findIndex(
            wrappedQuestion.value,
            item => item.displayId === displayId
          );
          const newValue = _.cloneDeep(wrappedQuestion.value);
          newValue[answerIndex] = {
            ...newValue[answerIndex],
            ...newProps
          };
          wrappedQuestion.value = {
            [wrappedQuestion.valueType]: newValue
          };
        }
        break;
      default:
        break;
    }
    return newForm;
  }
};

const getQuestionListByPage = page =>
  _.flatten(_.map(page.sections, section => section.questions));

const getValidatedQuestionListByPage = page =>
  _.filter(
    getQuestionListByPage(page),
    question => question.isMandatory || !_.isEmpty(question.validationRule)
  );

const getQuestionListCompleteness = ({
  questionList,
  valueMap,
  validationMap,
  hasErrorCheck = true,
  isStepper = false
}) => {
  const getCompleteStatus = question => {
    let completed = true;
    let error = false;
    // Exception
    if (!hasErrorCheck && !isStepper) {
      completed = true;
      return { completed, error };
    }
    // Exception
    if (!hasErrorCheck && isStepper) {
      completed = false;
      return { completed, error };
    }
    // If question has validationRule
    // it should check the question has no error is validationRuleMap first
    if (!_.isEmpty(question.validationRule)) {
      // ! NORMAL FLOW
      completed = _.get(validationMap, question.displayId, false) === false;
    }
    // If question isMandatory
    // check if vR is also passed or not
    if (question.isMandatory) {
      completed = completed && _.has(valueMap, question.displayId);
      error = !completed;
      return { completed, error };
    }
    // If question is TextField
    const questionAnswers = new questionUtils.QuestionAnswer({
      ...question.answers,
      questionType: question.questionType
    });
    const propsValue = new questionUtils.AnswerMapper({
      value: _.get(valueMap, `${question.displayId}.value`, ""),
      questionType: question.questionType
    });
    switch (question.questionType) {
      case TEXT_INPUT:
        // Empty String Checking
        if (questionUtils.TEXT_INPUT.isEmpty(propsValue.value)) {
          error = questionUtils.TEXT_INPUT.isEmpty(propsValue.value);
          completed = completed && !error;
          return { completed, error };
        }
        // Type || Format Checking
        switch (questionAnswers.formatProps.type) {
          case EMAIL:
            error = !questionUtils.TEXT_INPUT.isEmail(propsValue.value);
            completed = completed && !error;
            break;
          case NUMERIC:
            error = !questionUtils.TEXT_INPUT.isNumeric(propsValue.value);
            completed = completed && !error;
            break;
          case NRIC:
            error = !questionUtils.TEXT_INPUT.isNRIC(propsValue.value);
            completed = completed && !error;
            break;
          case CHARACTER:
            error = !questionUtils.TEXT_INPUT.isCharacter(propsValue.value);
            completed = completed && !error;
            break;
          default:
            break;
        }
        break;
      default:
        break;
    }
    return {
      completed,
      error
    };
  };
  const questionStatusList = _.map(questionList, question => {
    const { completed, error } = getCompleteStatus(question);
    return {
      displayId: question.displayId,
      completed,
      error
    };
  });
  return {
    completed: !_.some(questionStatusList, { completed: false }),
    error: _.some(questionStatusList, { error: true })
  };
};

export default {
  init,
  getForm,
  getFlattenFormItems,
  getReferenceDataRule,
  getQuestionIndex,
  getTypeByDisplayId,
  getParentDisplayIdByType,
  getQuestionListByPage,
  getValidatedQuestionListByPage,
  getQuestionListCompleteness,
  editAndUpdateForm
};
