export const TEXT = "text";
export const ARRAY = "array";
export const ARRAYOBJECT = "array_object";
export const OBJECT = "object";

/** DATE */
export const START_DATE = "startDate";
export const END_DATE = "endDate";
export const START_TIME = "startTime";
export const END_TIME = "endTime";
