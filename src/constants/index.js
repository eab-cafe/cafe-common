import * as answerType from "./answerType";
import * as formBehaviour from "./formBehaviour";
import * as questionType from "./questionType";

export default {
  answerType,
  formBehaviour,
  questionType
};

export { answerType, formBehaviour, questionType };
