import _ from "lodash";
import moment from "moment";
import {
  // #region CONSTANTS questionTypes
  TEXT_INPUT,
  BUTTON,
  SELECTOR,
  CHECK_BOX,
  DROP_DOWN,
  TOGGLE,
  DATE,
  SCALE,
  SLIDER,
  FILE_UPLOAD,
  CALC_INPUT,
  BASIC,
  ADVANCED,
  PLAIN_TEXT,
  HANDWRITING,
  CONDITIONAL_CHOICE,
  CONDITIONAL_CHOICE_2nd,
  RANDOM_NUMBER,
  // #endregion
  // #region CONSTANTS question_subTypes
  INPUT_LINE,
  INPUT_FIELD,
  SINGLE_ANSWER,
  MUITIPLE_ANSWER,
  CUSTOM,
  COUNTRY,
  INPUT_DATE,
  INPUT_TIME,
  INPUT_DATE_N_TIME,
  INPUT_TWOSTEP,
  INPUT_THREESTEP,
  // ADD,
  // SUBTRACT,
  SLIDER_BASIC,
  SLIDER_BASIC_L,
  SLIDER_BASIC_LV,
  SLIDER_SCALES,
  SLIDER_CV
  // #endregion
} from "../constants/questionType";

import {
  // #region
  TEXT,
  ARRAY,
  OBJECT,
  ARRAYOBJECT,
  START_DATE,
  END_DATE,
  START_TIME,
  END_TIME
  // #endregion
} from "../constants/answerType";

const throwErr = str => {
  throw new Error(str);
};

const validateQuestionType = type => {
  if (
    [
      TEXT_INPUT,
      BUTTON,
      SELECTOR,
      CHECK_BOX,
      DROP_DOWN,
      TOGGLE,
      DATE,
      SCALE,
      SLIDER,
      FILE_UPLOAD,
      CALC_INPUT,
      PLAIN_TEXT,
      HANDWRITING,
      CONDITIONAL_CHOICE,
      CONDITIONAL_CHOICE_2nd,
      RANDOM_NUMBER
    ].includes(type)
  ) {
    return type;
  }
  throwErr(`questionUtils:: Invalid questionType:: ${type}`);
};

class AnswerMapper {
  constructor({
    questionType,
    type = null,
    value = null,
    optionalValue = null
  }) {
    this._questionType = questionType;
    this._type = type;
    this._value = value;
    this._optionalValue = optionalValue;
  }
  set value(val) {
    this._value = val;
  }

  get value() {
    return this._value;
  }

  set type(val) {
    this._type = val;
  }

  get type() {
    return this._type;
  }

  set optionalValue(val) {
    this._optionalValue = val;
  }

  get optionalValue() {
    return this._optionalValue;
  }

  get optionalValueType() {
    return TEXT;
  }

  get valueType() {
    switch (this._questionType) {
      case DROP_DOWN:
      case SELECTOR:
      case TOGGLE:
      case SLIDER:
      case BUTTON:
        return ARRAY;
      case CONDITIONAL_CHOICE:
      case CALC_INPUT:
      case DATE:
        return ARRAYOBJECT;
      case PLAIN_TEXT:
      case HANDWRITING:
        return OBJECT;
      case TEXT_INPUT:
      default:
        return TEXT;
    }
  }

  get isWritable() {
    switch (this._questionType) {
      case DROP_DOWN:
      case SELECTOR:
      case TOGGLE:
      case DATE:
      case TEXT_INPUT:
      case BUTTON:
        return true;
      case CONDITIONAL_CHOICE:
      case CALC_INPUT:
      case PLAIN_TEXT:
      case HANDWRITING:
      default:
        return false;
    }
  }

  get isDisplayId() {
    switch (this.valueType) {
      case ARRAY:
      case ARRAYOBJECT:
        return true;
      case OBJECT:
      case TEXT:
      default:
        return false;
    }
  }

  ToMap = () => {
    let result = {
      value: this.value,
      valueType: this.valueType,
      type: this.type,
      isDisplayId: this.isDisplayId
    };
    if (!_.isEmpty(this.optionalValue)) {
      result = {
        ...result,
        optionalValue: this.optionalValue,
        optionalValueType: this.optionalValueType
      };
    }
    return result;
  };
}

class QuestionAnswer {
  constructor({
    questionType, // pass the questionType to here for validation
    question, // pass the original question to here for rewriting question.answers at final stage
    value = {}, // value object, contains TEXT, ARRAY, OBJECT properties respectively
    type = null, // answerType, if the question has answerType indentification pass it here
    formatProps = {}, // formatting properties, including validation, customization of error message etc
    styleProps = {}, // styling properties, reserved for CSS customization of each answers
    optionalProps = null // optional properties, only applicable if there are "optional" fields
  }) {
    this._value = value;
    this._question = question;
    this._questionType = validateQuestionType(questionType);
    this._type = type;
    this._formatProps = formatProps;
    this._styleProps = styleProps;
    this._optionalProps = optionalProps;
  }

  update = ({ ...props }) => {
    this._value = Object.assign({}, this._value, _.get(props, "value", {}));
    this._type = _.get(props, "type", this._type);
    this._formatProps = Object.assign(
      {},
      this._formatProps,
      _.get(props, "formatProps", {})
    );
    this._styleProps = Object.assign(
      {},
      this.styleProps,
      _.get(props, "styleProps")
    );
    this._optionalProps = Object.assign(
      {},
      this._optionalProps,
      _.get(props, "optionalProps")
    );
    this.WriteToQuestion();
  };
  // #region value setter getter
  get value() {
    switch (this._questionType) {
      case DROP_DOWN:
      case SELECTOR:
      case TOGGLE:
      case BUTTON:
        return this.valueArray;
      case SLIDER:
        switch (this._type) {
          case SLIDER_CV:
            return this.valueArray;
          default:
            return this.valueObject;
        }
      case CALC_INPUT:
        switch (this._type) {
          case BASIC:
            return this.valueArrayObject;
          case ADVANCED:
            return this.valueObject;
          default:
            return this.valueArrayObject;
        }
      case CONDITIONAL_CHOICE:
      case DATE:
        return this.valueArrayObject;
      case PLAIN_TEXT:
      case TEXT_INPUT:
      case HANDWRITING:
        return this.valueObject;
      default:
        return this.valueText;
    }
  }
  get valueType() {
    switch (this._questionType) {
      case DROP_DOWN:
      case SELECTOR:
      case TOGGLE:
      case BUTTON:
        return ARRAY;
      case SLIDER:
        switch (this._type) {
          case SLIDER_CV:
            return ARRAY;
          default:
            return OBJECT;
        }
      case CALC_INPUT:
        switch (this._type) {
          case BASIC:
            return ARRAYOBJECT;
          case ADVANCED:
            return OBJECT;
          default:
            return ARRAYOBJECT;
        }
      case CONDITIONAL_CHOICE:
      case DATE:
        return ARRAYOBJECT;
      case PLAIN_TEXT:
      case TEXT_INPUT:
      case HANDWRITING:
        return OBJECT;
      default:
        return TEXT;
    }
  }

  get values() {
    return this._value;
  }
  get valueText() {
    return this._value[TEXT];
  }
  get valueArray() {
    return this._value[ARRAY];
  }
  get valueObject() {
    return this._value[OBJECT];
  }
  get valueArrayObject() {
    return this._value[ARRAYOBJECT];
  }
  set value({ ...val }) {
    if (
      !_.has(val, TEXT) &&
      !_.has(val, ARRAY) &&
      !_.has(val, OBJECT) &&
      !_.has(val, ARRAYOBJECT)
    ) {
      throwErr(`questionUtils:: QuestionAnswer:: valueType not exist`);
    }
    this._value = {
      ...this._value,
      ...val
    };
    this.WriteToQuestion();
  }
  set valueText(val) {
    this.value = {
      [TEXT]: val
    };
  }
  set valueArray(val) {
    this.value = {
      [ARRAY]: val
    };
  }
  set valueObject(val) {
    this.value = {
      [OBJECT]: val
    };
  }
  set valueArrayObject(val) {
    this.value = {
      [ARRAYOBJECT]: val
    };
  }
  // #region type setter getter
  get type() {
    // validation, fallback
    switch (this._questionType) {
      case TEXT_INPUT:
        return [INPUT_LINE, INPUT_FIELD].indexOf(this._type) > -1
          ? this._type
          : INPUT_LINE;
      case SELECTOR:
        return [SINGLE_ANSWER, MUITIPLE_ANSWER].indexOf(this._type) > -1
          ? this._type
          : SINGLE_ANSWER;
      case DROP_DOWN:
        return [CUSTOM, COUNTRY].indexOf(this._type) > -1 ? this._type : CUSTOM;
      case DATE:
        return [INPUT_DATE, INPUT_TIME, INPUT_DATE_N_TIME].indexOf(this._type) >
          -1
          ? this._type
          : INPUT_DATE;
      case TOGGLE:
        return [INPUT_TWOSTEP, INPUT_THREESTEP].indexOf(this._type) > -1
          ? this._type
          : INPUT_TWOSTEP;
      case CALC_INPUT:
        return [BASIC, ADVANCED].indexOf(this._type) > -1 ? this._type : BASIC;
      case SLIDER:
        return [
          SLIDER_BASIC,
          SLIDER_BASIC_L,
          SLIDER_BASIC_LV,
          SLIDER_SCALES,
          SLIDER_CV
        ].indexOf(this._type) > -1
          ? this._type
          : SLIDER_BASIC;
      case BUTTON:
        return this._type;

      default:
        break;
    }
    return this._type;
  }
  set type(val) {
    this._type = val;
    this.WriteToQuestion();
  }
  // #endregion
  // #region formatProps setter getter
  get formatProps() {
    return this._formatProps;
  }

  set formatProps({ ...newFormatProps }) {
    this._formatProps = {
      ...this.formatProps,
      ...newFormatProps
    };
    this.WriteToQuestion();
  }
  // #endregion

  // #region styleProps setter getter
  get styleProps() {
    return this._styleProps;
  }
  set styleProps({ ...newStyleProps }) {
    this._styleProps = {
      ...this.styleProps,
      ...newStyleProps
    };
    this.WriteToQuestion();
  }
  // #endregion

  // #region optionalProps setter getter
  get optionalProps() {
    return this._optionalProps;
  }
  set optionalProps({ ...newOptionalProps }) {
    this._optionalProps = {
      ...this._optionalProps,
      ...newOptionalProps
    };
    this.WriteToQuestion();
  }
  // #endregion

  WriteToQuestion = () => {
    if (this._question) {
      this._question.answers = {
        value: this._value,
        type: this._type,
        formatProps: this._formatProps,
        styleProps: this._styleProps,
        optionalProps: this._optionalProps
      };
      // if (this._questionType === TEXT_INPUT) {
      // console.warn("WriteToQuestion:: this._question.answers");
      // console.log(this._question.answers);
      // }
    }
  };
}

// #region Formatting / Validation
// #region TEXT_INPUT || QuestionTextField
export const getMaxLength = maxLength => {
  if (Number.isNaN(maxLength)) {
    return null;
  }
  return Number(maxLength);
};

const isPassRegex = ({ regex, inputString }) => regex.test(inputString);

export const isEmail = inputString => {
  const regexEmail = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
  // return regexEmail.test(inputString);
  return isPassRegex({ regex: regexEmail, inputString });
};

export const isCharacter = inputString =>
  isPassRegex({ regex: /$|[a-zA-Z ]+/, inputString });
// /$|[a-zA-Z ]+/.test(inputString);

export const isNRIC = inputString =>
  isPassRegex({ regex: /^[STFG]\d{7}[A-Z]$/, inputString });

export const isEmpty = inputString => _.isEmpty(inputString.trim());

export const isNumeric = inputString => !isNaN(inputString);
// #endregion
// #region CALC_INPUT || QuestionCalculatedField
export const getAddition = (value, valueToAdd) =>
  Number(value) + Number(valueToAdd);

export const getSubtraction = (value, valueToSubtract) =>
  Number(value) - Number(valueToSubtract);
// #endregion
// #region
// #region GENERAL
export const isInValueMap = ({ valueMap, displayId }) =>
  _.has(valueMap, displayId);

export const getValueMapById = ({ valueMap, displayId, defaultValue }) =>
  _.get(valueMap, displayId, defaultValue);

export const getFormattedNumber = (
  value,
  format = /(\d{1,3})(?=(\d{3})+(?:$|\.))/g,
  replacement = "$1,"
) => String(value).replace(format, replacement);

export const getValueAnswerById = (answer, questionAnswer) => {
  const getLabelFromArray = displayId => {
    const target = _.find(_.get(questionAnswer, "value", []), {
      displayId
    });
    return _.get(target, "text", "");
  };
  let { value: displayIdStrings } = answer;
  displayIdStrings = _.flatten([displayIdStrings]).sort();
  const result = _.map(displayIdStrings, displayId =>
    getLabelFromArray(displayId)
  );
  // Optional Choices
  if (_.get(answer, "optionalValue")) {
    if (!_.isEmpty(answer.optionValue)) {
      result.push(answer.optionValue);
    }
  }
  return result.join(", ");
};

export const onChangeQuestionType = ({
  questionType,
  newQuestionType,
  onByPass,
  onIntercept
}) => {
  switch (questionType) {
    case DROP_DOWN:
    case TOGGLE:
    case SELECTOR:
      switch (newQuestionType) {
        case DROP_DOWN:
        case TOGGLE:
        case SELECTOR:
          onByPass();
          break;
        default:
          onIntercept();
          break;
      }
      break;
    default:
      onIntercept();
      break;
  }
};

// #endregion
// #region PLAIN_TEXT || QuestionPlainText

// TODO:: TextField
// TODO:: Please see if it is still applicable
// TODO:: As I dont see any prefix and suffix anymore
// TODO:: Please add to TextField validation if there are any
const getFormattedText = (answer, option) => {
  const { prefix = "", postfix = "" } = option;
  const { value: answerValue = "" } = answer;
  return `${prefix}${answerValue}${postfix}`;
};

// TODO:: Conditional
const getConditionalTextDtl = (answer, questionAnswer, patch) => {
  const result = [];
  const { value: answerValue = "" } = answer;
  const answers = _.get(questionAnswer, patch, []);
  if (answers && answers.length > 0) {
    _.forEach(answers, ans => {
      const curAns = _.find(answerValue, { displayId: ans.displayId });
      if (curAns) {
        result.push(curAns.text);
      }
    });
  }
  return result.join(", ");
};
const getConditionalText = (
  answer,
  questionAnswer,
  questionType,
  questionMap
) => {
  if (questionType === CONDITIONAL_CHOICE_2nd) {
    const { value: answerValue = "" } = questionAnswer;
    const questionAnswerTemp = _.find(questionMap, {
      displayId: answerValue[TEXT]
    });
    return getConditionalTextDtl(
      answer,
      questionAnswerTemp,
      "answers.value.array_object"
    );
  } else {
    return getConditionalTextDtl(answer, questionAnswer, "value.array_object");
  }
};

// TODO:: Date
// TODO:: Please add to Date validation if there are any
// ! Looking for better format
const getFormattedDate = dateTime => {
  const dateFormat = "DD/MM/YYYY";
  const displayDateFormat = dateFormat;
  const timeFormat = "HH:mm";
  const displayTimeFormat = "hh:mm A";

  let startDate = _.get(_.find(dateTime, { type: START_DATE }), "value", null);
  let endDate = _.get(_.find(dateTime, { type: END_DATE }), "value", null);
  let startTime = _.get(_.find(dateTime, { type: START_TIME }), "value", null);
  let endTime = _.get(_.find(dateTime, { type: END_TIME }), "value", null);

  // const isValidDate = param =>
  //   _.has(date, param) && moment(date[param], dateFormat, true).isValid();
  const isValidTime = (time, param) =>
    _.has(time, param) && moment(time[param], timeFormat, true).isValid();

  startDate = _.isEmpty(startDate)
    ? ""
    : moment(moment.unix(startDate).toISOString()).format(displayDateFormat);
  endDate = _.isEmpty(endDate)
    ? ""
    : moment(moment.unix(endDate).toISOString()).format(displayDateFormat);
  startTime = isValidTime("startTime")
    ? moment(startTime, timeFormat).format(displayTimeFormat)
    : "";
  endTime = isValidTime("endTime")
    ? moment(endTime, timeFormat).format(displayTimeFormat)
    : "";

  const connector = (startDate && endDate) || (startTime && endTime) ? "-" : "";

  return `${startDate} ${startTime} ${connector} ${endDate} ${endTime}`;
};

// #endregion

export default {
  // Class
  AnswerMapper,
  QuestionAnswer,
  // Validation || Functions
  [TEXT_INPUT]: {
    getMaxLength,
    isEmail,
    isNRIC,
    isCharacter,
    isNumeric,
    isEmpty
  },
  [CALC_INPUT]: {
    getAddition,
    getSubtraction
  },
  [DATE]: {
    // getDateByFormat?
  },
  [PLAIN_TEXT]: {
    getFormattedText, // ! THIS SHOULD BE HANDLED by TEXT_INPUT
    getFormattedDate, // ! THIS SHOULD BE HANDLED by DATE
    getConditionalText // ! THIS SHOULD BE HANDLED by Conditional
  },
  onChangeQuestionType,
  isInValueMap,
  isNumeric,
  getFormattedNumber,
  getValueMapById,
  getValueAnswerById
};
