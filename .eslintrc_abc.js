module.exports = {
  "extends": ["airbnb", "prettier"],
  "plugins": ["prettier"],
  "rules": {
    "prettier/prettier": "error",
    /*TODO for reducer reassign to state. remove or edit it when eslint update*/
    "no-param-reassign": [0],
    "no-eval": 0,
    /**
     * ignore string
     * */
    "max-len": [
      "error",
      {
        "code": 80,
        "ignoreTemplateLiterals": true,
        "ignoreStrings": true,
        "ignoreTrailingComments": true,
        "ignoreComments": true
      }
    ]
  }
};
