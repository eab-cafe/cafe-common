
## Available Scripts

In the project directory, you can run:
Noted that you can use ``yarn`` as an alternatives.

### `npm start`

Reserved

### `npm run build`

Builds the js from ``/src`` to ``/lib``  


### `npm run smartbuild`

Run ``npm run build`` && ``npm run smartbuild:admin`` && ``npm run smartbuild:interface``

### `npm run smartbuild:admin`

Copy the ``/lib`` to the ``Cafe`` `node_modules`,  
ensure the ``cafe`` project are in same scope with ``cafe-common`` and named `Cafe`

### `npm run smartbuild:interface`

Copy the ``/lib`` to the ``cafe-interface-engine`` `node_modules`,  
ensure the ``cafe-interface-engine`` project are in same scope with ``cafe-common`` and named `cafe-interface-engine`

### `npm run lint`

Run ``eslint``