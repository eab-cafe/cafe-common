{
      "env": {
            "browser": true,
            "es6": true,
            "node": true
      },
      "extends": [
            "eslint:recommended",
            "plugin:import/errors",
            "prettier"
      ],
      "globals": {
            "Atomics": "readonly",
            "SharedArrayBuffer": "readonly"
      },
      "parser": "babel-eslint",
      "parserOptions": {
            "ecmaFeatures": {
                  "jsx": true
            },
            "ecmaVersion": 2018,
            "sourceType": "module"
      },
      "plugins": [
            "import",
            "prettier"
      ],
      "rules": {
            "prettier/prettier": "error",
            "prefer-const": "error",
            "no-const-assign": "error",
            "no-var": "error",
            "no-new-object": "error",
            "object-shorthand": "error",
            "quote-props": [
                  "error",
                  "as-needed"
            ],
            "no-prototype-builtins": "error",
            "no-array-constructor": "error",
            "array-callback-return": "error",
            "prefer-destructuring": "error",
            "prefer-template": "error",
            "template-curly-spacing": "error",
            "no-eval": "error",
            "no-useless-escape": "error",
            "wrap-iife": [
                  "error",
                  "any"
            ],
            "no-loop-func": "error",
            "prefer-rest-params": "error",
            "prefer-spread": "error",
            "prefer-arrow-callback": "error",
            "arrow-spacing": "error",
            "arrow-body-style": "error",
            "no-confusing-arrow": "error",
            "no-dupe-class-members": "error",
            "no-duplicate-imports": "error",
            "import/no-mutable-exports": "error",
            "import/first": "error",
            "import/no-webpack-loader-syntax": "error",
            "no-iterator": "error",
            "no-restricted-syntax": "error",
            "generator-star-spacing": [
                  "error",
                  {
                        "before": false,
                        "after": true
                  }
            ],
            "dot-notation": "error",
            "no-restricted-properties": "error",
            "no-undef": "error",
            "one-var": [
                  "error",
                  "never"
            ],
            "no-multi-assign": "error",
            "no-plusplus": "error",
            "max-len": [
                  "error",
                  {
                        "code": 80,
                        "ignoreRegExpLiterals": true,
                        "ignoreTemplateLiterals": true,
                        "ignoreStrings": true,
                        "ignoreTrailingComments": true,
                        "ignoreComments": true
                  }
            ],
            "no-unused-vars": "error",
            "no-case-declarations": "error",
            "no-nested-ternary": "error",
            "no-unneeded-ternary": "error",
            "spaced-comment": [
                  "error",
                  "always"
            ],
            "eol-last": "error",
            "newline-per-chained-call": "error",
            "no-whitespace-before-property": "error",
            "no-multiple-empty-lines": "error",
            "space-in-parens": "error",
            "array-bracket-spacing": "error",
            "block-spacing": "error",
            "computed-property-spacing": "error",
            "func-call-spacing": "error",
            "comma-style": "error",
            "no-useless-constructor": "error"
      },
      "settings": {
            "propWrapperFunctions": [
                  "forbidExtraProps",
                  {
                        "property": "freeze",
                        "object": "Object"
                  },
                  {
                        "property": "myFavoriteWrapper"
                  }
            ],
            "import/resolver": {
                  "node": {
                        "paths": [
                              "node_modules",
                              "src"
                        ],
                        "moduleDirectory": [
                              "node_modules"
                        ]
                  }
            }
      }
}