import _ from "lodash";
import {
  // Behaviours
  REDIRECT_EXTERNAL_LINK,
  REDIRECT_CAFE_PAGE,
  SUBMIT,
  // Conditions
  PAGE_PRECHECK
} from "../constants/formBehaviour";
import { getPageList } from "./commonUtils";
import formUtils from "./formUtils";

export class BehaviourController {
  constructor({
    form,
    page,
    pageIndex,
    setSubmit,
    setPageIndex,
    setNextClickedList,
    setCheckError,
    valueMap = {},
    checkedList = [],
    visibilityMap = {},
    validationMap = {},
    hasErrorCheck = true
  }) {
    this._form = form;
    this._page = page;
    this._pageIndex = pageIndex;
    this._setPageIndex = setPageIndex;
    this._setSubmit = setSubmit;
    this._setNextClickedList = setNextClickedList;
    this._valueMap = valueMap;
    this._checkedList = checkedList;
    this._visibilityMap = visibilityMap;
    this._validationMap = validationMap;
    this._hasErrorCheck = hasErrorCheck;
    this._setCheckError = setCheckError;
  }

  set currentPage(index) {
    const pageList = getPageList(this._form);
    this._page = pageList[index];
    this._pageIndex = index;
  }

  set checkedList(list) {
    this._checkedList = list;
  }

  set condition(condition) {
    this._condition = condition;
  }

  get isPassCondition() {
    switch (this._condition) {
      case PAGE_PRECHECK: {
        const questionList = formUtils.getValidatedQuestionListByPage(
          this._page
        );
        const { completed } = formUtils.getQuestionListCompleteness({
          questionList,
          valueMap: this._valueMap,
          validationMap: this._validationMap,
          hasErrorCheck: this._hasErrorCheck
        });
        return completed;
      }
      default:
        return true;
    }
  }

  set pageIndexDisplayId(displayId) {
    const pageList = getPageList(this._form);
    const pageIndex = _.findIndex(pageList, { displayId });
    this.pageIndex = pageIndex;
  }

  set pageIndex(index) {
    this.clickNextButton();
    if (this.isPassCondition) {
      this._setPageIndex(index);
    }
    // Check it if there are behaviours
    this._setCheckError(true);
  }

  // No matter the condition fulfilled or not,
  // Once the button is clicked,
  // it should validate.
  clickNextButton = () => {
    const newCheckedList = [...this._checkedList];
    newCheckedList[this._pageIndex] = true;
    this._setNextClickedList([...newCheckedList]);
  };

  submitForm = () => {
    this.clickNextButton();
    if (this.isPassCondition) {
      this._setSubmit(true);
    }
  };
}

export class Behaviour {
  constructor({ type, controller = null, href = null, condition = null }) {
    this._type = type;
    this._controller = controller;
    this._href = href;
    this._condition = condition;
  }

  set controller(controller) {
    this._controller = controller;
  }

  get controller() {
    return this._controller;
  }

  get isValid() {
    switch (this._type) {
      case REDIRECT_CAFE_PAGE:
        return !_.isEmpty(this._href) || !_.isEmpty(this._controller);
      case REDIRECT_EXTERNAL_LINK:
        return !_.isEmpty(this._href);
      case SUBMIT:
        return true;
      default:
        return false;
    }
  }

  // Pass the parameters required in order to run condition checking.
  run = () => {
    if (!this.isValid) {
      throw new Error("behaviourUtils:: Invalid Format on required parameters");
    }
    this._controller.condition = this._condition;
    switch (this._type) {
      case REDIRECT_EXTERNAL_LINK:
        window.open(this._href, "_blank");
        break;
      case REDIRECT_CAFE_PAGE:
        this._controller.pageIndexDisplayId = this._href;
        break;
      case SUBMIT:
        this._controller.submitForm();
        break;
      default:
        break;
    }
  };
}

export default {
  BehaviourController,
  Behaviour
};
